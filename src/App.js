import React from 'react';
import './App.css'
import {
  Navbar,
  Nav,
  Button,
  Row,
  Col,
  Image,
  Card,
  Jumbotron,
  Container
} from 'react-bootstrap';


function Header(){
  return(
    
   <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/header_logo-a02452e14625b5af8dbcb37a3cc3ffbb.svg"
            width="48"
            height="48"
            color="dark"
            className="img d-inline-block  float-left align-top"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
            <Nav.Link href="#home">Pelajaran</Nav.Link>
            <Nav.Link href="#link">Slide Library</Nav.Link>
            <Nav.Link href="#link">Peringkat</Nav.Link>
            <Nav.Link href="#link">Bantuan</Nav.Link>
          </Nav>
        </Navbar.Collapse>
          <img
            alt="wahyusnjy (Lv.27)"
            src="https://lh3.googleusercontent.com/a-/AOh14Gj9l-kepjtATdAWyB8tyQ_3nM5MEvjYRbIYEOjj=s96-c?sz=150"
            width="33"
            height="33"
            className="align-top float-right rounded-circle"
          />

      </Navbar>
    );
}
        

        function Kotak(){
          return(
          <div className="jumbotron">
            <Jumbotron>
          <img
            src="https://d18en44nkeqroq.cloudfront.net/paths/node/top_image.svg"
            width="280"
            height="280"
            className="float-right "
          />
            <h1>Path Pengembangan<h1>Web (Node.JS)</h1></h1>
          
        <p>Di path ini,Anda akan mempelajari seluruh ilmu penting untuk
        <p>mengembangkan website.
        <p>Dengan menggunakan Node.js,Anda akan belajar cara kerja aplikasi web
        <p>dan membuat aplikasi web
        </p></p></p></p>
          <p>
             <Button variant="primary" href="https://progate.com/paths/node">Learn more</Button>
          </p>
           </Jumbotron>
            </div>
          );
        }

    function Box(props){
      const Fitur = (props) => {
      return(
          <Container>
          <div className="udin col-sm-12 col-lg-3 col-sm-4">
           <Card style={{ width: '18rem' }}>
            <Card.Title>{props.title}</Card.Title>
          <Card.Body>
          <Card.Img  src={props.image} />
            <Card.Text>
              {props.subtitle}
            </Card.Text>
            <Button href={props.href}>Klik disini</Button>
          </Card.Body>
        </Card>
        </div>
      </Container>  
      );
    }

      return (
      <Row>
        <Col>
          <Fitur
            title="HTML & CSS"
            subtitle="Bahasa digunakan untuk membuat dan mendesain tampilan setiap situs web"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/html-d55f9f9c51ce7c499c71c8c50d012164782602def8da6e623d50bc523e71fe3d.svg"
            href="https://progate.com/languages/html"  
          />
        </Col>

        <Col>
          <Fitur
            title="JavaScript"
            subtitle="Bahasa fleksibel yang digunakan dimana saja, mulai dari situs web interaktif hingga server backend"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/es6-f27d15ffcd93edaaf6ade3c76c857d6890ade85e91fee07fb5a39458b2195c9c.svg"
            href="https://progate.com/languages/es6"
          />
        </Col>

        <Col>
          <Fitur
            title="Node.js"
            subtitle="Runtime Environment JavaScript untuk mengembangkan aplikasi server-side"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/nodejs-09daf8af1cfc5ca33f450678cb7d952f1c52d9633159d4979ba3c14ffa3cfd48.svg"
            href="https://progate.com/languages/nodejs"
          />
        </Col>
        <Col>
          <Fitur
            title="React"
            subtitle="Library JavaScript front-end yang banyak digunakan oleh programmer global"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/react-f6bedc6780093fd50ce0ba62fc5ba17e165a4fc3e32c97380dcb43703fbac623.svg"
             href="https://progate.com/languages/react"
          />
        </Col>
        <Col>
          <Fitur
            title="SQL"
            subtitle="Bahasa database digunakan untuk manipulasi dan analisis database"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/sql-8161d8089f9e5dfd60d1503a5f898dc573c03b60fe83ad7acfb605d1078475d7.svg"
              href="https://progate.com/languages/sql"
          />
        </Col>
        <Col>
          <Fitur
            title="Python"
            subtitle="Sebuah bahasa pemrograman populer yang digunakan untuk data science, machine learning,dan banyak lagi."
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/python-baf258896a111d1b23043008e2ac835a63ea3f0349b0afaaf40b006c7414f8f0.svg"
             href="https://progate.com/languages/python"
          />
        </Col>
        <Col>
          <Fitur
            title="Ruby"
            subtitle="Bahasa yang dinamis,serba-guna biasanya digunakan untuk membangun aplikasi-aplikasi berbasis web"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/ruby-67da22c74d14d226cbb854339c9d0d797440abb257eec16e51a00d9abba75d97.svg"
             href="https://progate.com/languages/ruby"
          />
        </Col>
        <Col>
          <Fitur
            title="Java"
            subtitle="Bahasa populer yang bisa digunakan untuk Pengembangan perangkat lunak,aplikasi,seluler,dan lainnya"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/java-f8af3f7d7b90c6e74601328c9d717348fad01135d50c9ae06a547cd32a23f0f7.svg"
             href="https://progate.com/languages/java"
          />
        </Col>
        <Col>
          <Fitur
            title="PHP"
            subtitle="Bahasa server-side yang cocok digunakan untuk pengembangan web"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/php-4efa925c22077e637fb1c4e62af8b1acc5671c8ec70db932b77b35531c5f0772.svg"
             href="https://progate.com/languages/php"
          />
        </Col>
        <Col>
          <Fitur
            title="Sass"
            subtitle="Cara mudah menuliskan CSS"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/sass-1cf9cd102a27fe0dd13ce596810dc2a0c57a4ca875ea15d7e2c6b66963332f91.svg"
             href="https://progate.com/languages/sass"
          />
        </Col>
        <Col>
          <Fitur
            title="Git"
            subtitle="Sebuah sistem kontrol versi paling populer yang para pengembang gunakan untuk melacak dan membagikan code"
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/git-129fee5eb0f17652945edd80bdcfefab77542705d1465e7f4083d3e80a2ee9ae.svg"
             href="https://progate.com/languages/git"
          />
        </Col>
        <Col>
          <Fitur
            title="Command Line"
            subtitle="Ini adalah keahlian yang wajib dipelajari untuk dapat melakukan tugas-tugas melalui perintah komputer."
            image="https://d2aj9sy12tbpym.cloudfront.net/assets/languages/language_list/commandline-a75be113866b46af2aa57f64a185992ec4a70d0b4591d22bfbb297bb9f49da00.svg"
            href="https://progate.com/languages/commandline"
          />
        </Col>
      </Row>
      );
    }

    function Footer() {
      return(
          <div className="footer">
            <Container>
                  <Row>
                        <div className="col-sm-4">
                        <img
                          src="https://d2aj9sy12tbpym.cloudfront.net/assets/landing/primary_logo-4d1810538e410b4c6af84210420099ca1772e8cb39013fad8532e499bcdb136e.svg"
                            width=""
                            height=""
                        />
                        </div>
                        <div className="col-sm-2">
                        <h4>Layanan</h4>
                          <ul>
                             <li>Pelajaran</li>
                             <li>Paket</li>
                             <li>Dasbor</li>
                             <li>Kisah sukses</li>
                             <li>Blog</li>
                             <li>Bantuan</li>
                        </ul>
                        </div>

                        <div className="col-sm-2">
                        <h4>Dukungan</h4>
                        <ul>
                            <li>Tentang progate</li>
                            <li>Ketentuan</li>
                            <li>Kebijakan Privasi</li>
                        </ul>
                        </div>

                        <div className="col-sm-4">
                        <h4>Ikuti Kami di</h4>
                        <ul>
                            <li>Instagram</li>
                            <li>Twitter</li>
                            <li>Facebook</li>
                        </ul>
                        </div>
                  </Row>
            </Container>
          </div>
      );
    }

function App() {
  return (
  <Container>
  <div>
   <Header/>
   <Kotak/>
   <Box/>
   <Footer/>
   </div>
   </Container>
  );
}

export default App;
